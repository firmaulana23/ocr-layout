//
//  ViewController.swift
//  ocr layout
//
//  Created by Muhamad Firman Maulana on 19/07/22.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {

    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var overlay: UIImageView!
    
//    @IBOutlet weak var textView: UITextView!
    
    var captureSession: AVCaptureSession?
    let photoDataOutput = AVCapturePhotoOutput()
    var videoPreviewLayer = AVCaptureVideoPreviewLayer()
    var videoDataOutput = AVCaptureVideoDataOutput()
    
    let myLayer = CALayer()
    let maskImageView = UIImageView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.checkCameraPermission()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        videoPreviewLayer.frame = view.bounds
    
        self.setOverlay()
        
    }
    
    private func setOverlay(){
        view.backgroundColor = .white
        view.layer.addSublayer(videoPreviewLayer)
        
        maskImageView.contentMode = .center
        maskImageView.backgroundColor = UIColor(white: 1, alpha: 0.5)
        maskImageView.image = UIImage(named:"ProfileMask.png")
//        maskImageView.frame = CGRect(x: 20, y: 325, width: 350, height: 192)
        maskImageView.frame = view.bounds
        view.mask = maskImageView
        
//        overlay.image = UIImage(named:"Group5.png")
        let image = UIImage(named:"Group5.png")!
        var aspectR: CGFloat = 0.0
        aspectR = image.size.width/image.size.height

        overlay.translatesAutoresizingMaskIntoConstraints = false
        
        overlay.contentMode = .scaleAspectFit
        overlay.image = image
                NSLayoutConstraint.activate([
                    overlay.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                    overlay.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                    overlay.leadingAnchor.constraint(greaterThanOrEqualTo: view.leadingAnchor),
                    overlay.trailingAnchor.constraint(lessThanOrEqualTo: view.trailingAnchor),
                    overlay.heightAnchor.constraint(equalTo: overlay.widthAnchor, multiplier: 1/aspectR)
                ])
//        overlay.frame = CGRect(x: 20, y: 325, width: 350, height: 192)
        view.addSubview(overlay)
    }
    
    private func checkCameraPermission(){
        switch AVCaptureDevice.authorizationStatus(for: .video){
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [weak self] granted in
                guard granted else{
                    return
                }
                DispatchQueue.main.async {
                    self?.setUpCamera()
                }
            }
        case .restricted:
            break
        case .denied:
            break
        case .authorized:
            setUpCamera()
        @unknown default:
            break
        }
    }
    
    private func setUpCamera(){
        let captureSession = AVCaptureSession()
        if let videoDevices = AVCaptureDevice.default(for: .video){
            do {
                let cameraInput = try AVCaptureDeviceInput(device: videoDevices)
                if captureSession.canAddInput(cameraInput){
                    captureSession.addInput(cameraInput)
                }
                
                if captureSession.canAddOutput(videoDataOutput){
                    captureSession.addOutput(videoDataOutput)
                }
                
                videoPreviewLayer.videoGravity = .resizeAspectFill
                videoPreviewLayer.session = captureSession
                
                captureSession.startRunning()
                self.captureSession = captureSession
            }
            catch {
                print(error)
            }
        }
    }
    
}
